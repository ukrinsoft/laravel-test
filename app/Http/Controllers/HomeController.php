<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function showForm()
    {
        $table = [];
        $path = asset('storage/file.xml');
        $current_new = file_get_contents($path);
        if ($current_new != '') {
            $current_new = explode("\n", $current_new);
            foreach ($current_new as $val) {
                if ($val != '') {
                    $array = json_decode($val);
                    $table[] = $array;
                }
            }
        }

        if ($table != '') {
            $table = array_reverse($table);
        }
        $massage = '';
        return view('form', ['table' => $table, 'massage' => '']);
    }


    public function saveForm(Request $request)
    {
        $array['name'] = $request->name;
        $array['quantity'] = $request->quantity;
        $array['price'] = $request->price;
        $array['number'] = $request->price * $request->quantity;
        $array['submitted'] = date('Y-m-d H:i:s');
        $writeLine = json_encode($array);
        $table = [];
        $path = asset('storage/file.xml');
        $current = file_get_contents($path);
        $current .= $writeLine . "\n";
       $pash=str_replace('app/Http/Controllers','public/storage/file.xml',dirname(__FILE__));
        file_put_contents($pash, $current);
        $current_new = file_get_contents($path);
        if ($current_new != '') {
            $current_new = explode("\n", $current_new);
            foreach ($current_new as $val) {
                if ($val != '') {
                    $array = json_decode($val);
                    $table[] = $array;
                }
            }
        }
        if ($table != '') {
            $table = array_reverse($table);
        }
        $massage = 'successful save';
        return view('form', ['table' => $table, 'massage' => $massage]);
    }

}
